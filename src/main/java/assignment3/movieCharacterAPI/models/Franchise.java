package assignment3.movieCharacterAPI.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Franchise {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "franchise_id")
    private int franchise_id;
    @Column(name = "franchise_name")
    private String franchise_name;
    @Column(name = "description")
    private String description;
    @ManyToMany
    @JoinTable(
            name = "franchise_movies",
            joinColumns = {@JoinColumn(name = "franchise_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies;
    @ManyToMany(mappedBy = "franchises")
    private Set<MovieCharacter> characters;

    public Set<MovieCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<MovieCharacter> characters) {
        this.characters = characters;
    }

    public int getFranchise_id() {
        return franchise_id;
    }

    public void setFranchise_id(int franchise_id) {
        this.franchise_id = franchise_id;
    }

    public String getFranchise_name() {
        return franchise_name;
    }

    public void setFranchise_name(String franchise_name) {
        this.franchise_name = franchise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }
}