package assignment3.movieCharacterAPI.models;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Set;

@Getter
@Setter
@Entity
public class Movie {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "movie_id")
    private int movie_id;
    @Column(name = "movie_title")
    private String movie_title;
    @Column(name = "genre")
    private String genre;
    @Column(name = "release_year")
    private int release_year;
    @Column(name = "director")
    private String director;
    @Column(name = "picture_url")
    private String picture_url;
    @Column(name = "trailer_url")
    private String trailer_url;
    @ManyToMany(mappedBy = "movies")
    private Set<MovieCharacter> characters;
    @ManyToMany(mappedBy = "movies")
    private Set<Franchise> franchises;

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getMovie_title() {
        return movie_title;
    }

    public void setMovie_title(String movie_title) {
        this.movie_title = movie_title;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public int getRelease_year() {
        return release_year;
    }

    public void setRelease_year(int release_year) {
        this.release_year = release_year;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }


    public Set<MovieCharacter> getCharacters() {
        return characters;
    }

    public void setCharacters(Set<MovieCharacter> characters) {
        this.characters = characters;
    }

    public Set<Franchise> getFranchises() {
        return franchises;
    }

    public void setFranchises(Set<Franchise> franchises) {
        this.franchises = franchises;
    }
}

