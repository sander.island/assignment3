package assignment3.movieCharacterAPI.models;

import javax.persistence.*;
import java.util.Set;

@Entity
public class MovieCharacter {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "character_id")
    private int character_id;
    @Column(name = "full_name")
    private String full_name;
    @Column(name = "alias")
    private String alias;
    @Column(name = "gender")
    private String gender;
    @Column(name = "picture_url")
    private String picture_url;
    @ManyToMany
    @JoinTable(
            name = "movie_character_movies",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "movie_id")}
    )
    private Set<Movie> movies;
    @ManyToMany
    @JoinTable(
            name = "character_franchises",
            joinColumns = {@JoinColumn(name = "character_id")},
            inverseJoinColumns = {@JoinColumn(name = "franchise_id")}
    )
    private Set<Franchise> franchises;

    public Set<Franchise> getFranchises() {
        return franchises;
    }

    public void setFranchises(Set<Franchise> franchises) {
        this.franchises = franchises;
    }

    public int getCharacter_id() {
        return character_id;
    }

    public void setCharacter_id(int character_id) {
        this.character_id = character_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public Set<Movie> getMovies() {
        return movies;
    }

    public void setMovies(Set<Movie> movies) {
        this.movies = movies;
    }

    @Override
    public String toString() {
        return "MovieCharacter{" +
                "character_id=" + character_id +
                ", full_name='" + full_name + '\'' +
                ", alias='" + alias + '\'' +
                ", gender='" + gender + '\'' +
                ", picture_url='" + picture_url + '\'' +
                ", movie=" + movies +
                '}';
    }
}