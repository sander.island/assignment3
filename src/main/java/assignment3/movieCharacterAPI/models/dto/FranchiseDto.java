package assignment3.movieCharacterAPI.models.dto;

import lombok.Data;

import java.util.Set;

@Data
public class FranchiseDto {
    private int franchise_id;
    private String franchise_name;
    private String description;
    private Set<Integer> movie_ids;
    private Set<Integer> character_ids;

    public Set<Integer> getCharacter_ids() {
        return character_ids;
    }

    public void setCharacter_ids(Set<Integer> character_ids) {
        this.character_ids = character_ids;
    }

    public int getFranchise_id() {
        return franchise_id;
    }

    public void setFranchise_id(int franchise_id) {
        this.franchise_id = franchise_id;
    }

    public String getFranchise_name() {
        return franchise_name;
    }

    public void setFranchise_name(String franchise_name) {
        this.franchise_name = franchise_name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<Integer> getMovie_ids() {
        return movie_ids;
    }

    public void setMovie_ids(Set<Integer> movie_ids) {
        this.movie_ids = movie_ids;
    }
}
