package assignment3.movieCharacterAPI.models.dto;

import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import lombok.Data;

import java.util.Set;

@Data
public class MovieDto {
    private int movie_id;
    private String director;
    private String genre;
    private String movie_title;
    private String picture_url;
    private int release_year;
    private String trailer_url;
    private Set<Integer> franchise_ids;
    private Set<Integer> character_ids;

    public int getMovie_id() {
        return movie_id;
    }

    public void setMovie_id(int movie_id) {
        this.movie_id = movie_id;
    }

    public String getDirector() {
        return director;
    }

    public void setDirector(String director) {
        this.director = director;
    }

    public String getGenre() {
        return genre;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public String getMovie_title() {
        return movie_title;
    }

    public void setMovie_title(String movie_title) {
        this.movie_title = movie_title;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public int getRelease_year() {
        return release_year;
    }

    public void setRelease_year(int release_year) {
        this.release_year = release_year;
    }

    public String getTrailer_url() {
        return trailer_url;
    }

    public void setTrailer_url(String trailer_url) {
        this.trailer_url = trailer_url;
    }

    public Set<Integer> getFranchise_ids() {
        return franchise_ids;
    }

    public void setFranchise_ids(Set<Integer> franchise_ids) {
        this.franchise_ids = franchise_ids;
    }

    public Set<Integer> getCharacter_ids() {
        return character_ids;
    }

    public void setCharacter_ids(Set<Integer> character_ids) {
        this.character_ids = character_ids;
    }
}
