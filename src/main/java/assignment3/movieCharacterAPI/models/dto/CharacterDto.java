package assignment3.movieCharacterAPI.models.dto;

import lombok.Data;

import java.util.Set;

@Data
public class CharacterDto {
    private int character_id;
    private String full_name;
    private String alias;
    private String gender;
    private String picture_url;
    private Set<Integer> movies;
    private Set<Integer> franchise_ids;

    public Set<Integer> getFranchise_ids() {
        return franchise_ids;
    }

    public void setFranchise_ids(Set<Integer> franchise_ids) {
        this.franchise_ids = franchise_ids;
    }


    public int getCharacter_id() {
        return character_id;
    }

    public void setCharacter_id(int character_id) {
        this.character_id = character_id;
    }

    public String getFull_name() {
        return full_name;
    }

    public void setFull_name(String full_name) {
        this.full_name = full_name;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }


    @Override
    public String toString() {
        return "CharacterDto{" +
                "character_id=" + character_id +
                ", full_name='" + full_name + '\'' +
                ", alias='" + alias + '\'' +
                ", gender='" + gender + '\'' +
                ", picture_url='" + picture_url + '\'' +
                ", movieCharacters=" + movies +
                '}';
    }

    public Set<Integer> getMovies() {
        return movies;
    }

    public void setMovies(Set<Integer> movies) {
        this.movies = movies;
    }
}
