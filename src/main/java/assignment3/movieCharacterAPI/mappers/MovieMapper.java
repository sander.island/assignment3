package assignment3.movieCharacterAPI.mappers;

import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.FranchiseDto;
import assignment3.movieCharacterAPI.models.dto.MovieDto;
import assignment3.movieCharacterAPI.services.characterService.CharacterService;
import assignment3.movieCharacterAPI.services.franchiseService.FranchiseService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class MovieMapper {

    @Autowired
    FranchiseService franchiseService;
    @Autowired
    CharacterService characterService;

    @Mapping(target = "character_ids", source = "characters", qualifiedByName = "characterToId")
    @Mapping(target = "franchise_ids", source = "franchises", qualifiedByName = "franchiseToId")
    public abstract MovieDto movieToMovieDto(Movie movie);
    public abstract Collection<MovieDto> movieToMovieDto(Collection<Movie> movie);

    @Mapping(target = "characters", source = "character_ids", qualifiedByName = "idToCharacter")
    @Mapping(target = "franchises", source = "franchise_ids", qualifiedByName = "idToFranchise")
    public abstract Movie movieDtoToMovie(MovieDto dto);
    public abstract Collection<Movie> movieDtoToMovie(Collection<MovieDto> dto);

    @Named("characterToId")
    Set<Integer> mapCharacterToId(Set<MovieCharacter> source) {
        if (source == null) {
            return null;
        }
        return source.stream().map(MovieCharacter::getCharacter_id).collect(Collectors.toSet());
    }

    @Named("idToCharacter")
    Set<MovieCharacter> mapIdsToCharacters(Set<Integer> id) {
        if( id == null){ return null; }
        return id.stream()
                .map( i -> characterService.findById(i))
                .collect(Collectors.toSet());
    }

    @Named("franchiseToId")
    Set<Integer> mapFranchiseToId(Set<Franchise> source) {
        if (source == null) {
            return null;
        }
        return source.stream().map(Franchise::getFranchise_id).collect(Collectors.toSet());
    }

    @Named("idToFranchise")
    Set<Franchise> mapIdsToFranchise(Set<Integer> id) {
        if( id == null){ return null; }
        return id.stream()
                .map( i -> franchiseService.findById(i))
                .collect(Collectors.toSet());
    }
}
