package assignment3.movieCharacterAPI.mappers;

import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.FranchiseDto;
import assignment3.movieCharacterAPI.services.characterService.CharacterService;
import assignment3.movieCharacterAPI.services.franchiseService.FranchiseService;
import assignment3.movieCharacterAPI.services.movieService.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.criteria.CriteriaBuilder;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class FranchiseMapper {
    @Autowired
    MovieService movieService;
    @Autowired
    CharacterService characterService;

    @Mapping(target = "movie_ids", source = "movies", qualifiedByName = "moviesToIds")
    @Mapping(target = "character_ids", source = "characters", qualifiedByName = "charactersToIds")
    public abstract FranchiseDto franchiseToFranchiseDto(Franchise franchise);
    public abstract Collection<FranchiseDto> franchiseToFranchiseDTO(Collection<Franchise> franchises);

    @Mapping(target = "movies", source = "movie_ids", qualifiedByName = "idsToMovies")
    @Mapping(target = "characters", source = "character_ids", qualifiedByName = "idsToCharacters")
    public abstract Franchise franchiseDtoToFranchise(FranchiseDto dto);
    public abstract Collection<Franchise> franchiseDtoToFranchise(Collection<FranchiseDto> dto);

    @Named("moviesToIds")
    Set<Integer> moviesToIds(Set<Movie> movies){
        if (movies == null) { return null; }
        return movies.stream().map(Movie::getMovie_id).collect(Collectors.toSet());
    }

    @Named("idsToMovies")
    Set<Movie> idsToMovies(Set<Integer> ids){
        if(ids == null) { return null; }
        return ids.stream().map(id -> movieService.findById(id)).collect(Collectors.toSet());
    }

    @Named("charactersToIds")
    Set<Integer> charactersToIds(Set<MovieCharacter> characters){
        if (characters == null) { return null; }
        return characters.stream().map(MovieCharacter::getCharacter_id).collect(Collectors.toSet());
    }

    @Named("idsToCharacters")
    Set<MovieCharacter> idsToCharacters(Set<Integer> ids){
        if(ids == null) { return null; }
        return ids.stream().map(id -> characterService.findById(id)).collect(Collectors.toSet());
    }
}
