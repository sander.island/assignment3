package assignment3.movieCharacterAPI.mappers;

import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.CharacterDto;
import assignment3.movieCharacterAPI.services.franchiseService.FranchiseService;
import assignment3.movieCharacterAPI.services.movieService.MovieService;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Mapper(componentModel = "spring")
public abstract class CharacterMapper {

    @Autowired
    private MovieService movieService;
    @Autowired
    private FranchiseService franchiseService;

    @Mapping(target = "movies", source = "movies", qualifiedByName = "moviesToIds")
    @Mapping(target = "franchise_ids", source = "franchises", qualifiedByName = "franchisesToIds")
    public abstract CharacterDto characterToCharacterDto(MovieCharacter movieCharacter);
    public abstract Collection<CharacterDto> characterToCharacterDTO(Collection<MovieCharacter> movieCharacters);


    @Mapping(target = "movies", source = "movies", qualifiedByName = "idsToMovies")
    @Mapping(target = "franchises", source = "franchise_ids", qualifiedByName = "idsToFranchises")
    public abstract MovieCharacter characterDtoToCharacter(CharacterDto dto);
    public abstract Collection<MovieCharacter> characterDtoToCharacter(Collection<CharacterDto> dto);

    @Named("moviesToIds")
    public Set<Integer> moviesToIds(Set<Movie> movie){
        if( movie == null){ return null; }
        return movie.stream().map(s -> s.getMovie_id()).collect(Collectors.toSet());
    }

    @Named("idsToMovies")
    public Movie idsToMovies(int id){
        return movieService.findById(id);
    }

    @Named("franchisesToIds")
    Set<Integer> franchisesToIds(Set<Franchise> franchises){
        if (franchises == null){ return null; }
        return franchises.stream().map(Franchise::getFranchise_id).collect(Collectors.toSet());
    }

    @Named("idsToFranchises")
    Set<Franchise> idsToFranchises(Set<Integer> ids){
        if (ids == null){ return null; }
        return ids.stream().map(id -> franchiseService.findById(id)).collect(Collectors.toSet());
    }
}