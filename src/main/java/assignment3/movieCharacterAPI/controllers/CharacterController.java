package assignment3.movieCharacterAPI.controllers;

import assignment3.movieCharacterAPI.mappers.CharacterMapper;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.CharacterDto;
import assignment3.movieCharacterAPI.models.dto.FranchiseDto;
import assignment3.movieCharacterAPI.services.characterService.CharacterService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;

/**
 * Character Controller class for managing GET, POST, PUT and DELETE requests
 */
@RestController
@RequestMapping(path = "characters")
public class CharacterController {
    private final CharacterService characterService;
    private final CharacterMapper characterMapper;

    /**
     * CharacterController constructor
     * @param characterService CharacterService
     * @param characterMapper CharacterMapper
     */
    public CharacterController(CharacterService characterService, CharacterMapper characterMapper) {
        this.characterService = characterService;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all characters.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No characters found.",
                    content = @Content)
    })
    @GetMapping // GET: localhost:8080/characters
    public ResponseEntity<Collection<CharacterDto>> getAll() {
        System.out.println(characterService.findAll());
        Collection<CharacterDto> characters = characterMapper.characterToCharacterDTO(characterService.findAll());
        return ResponseEntity.ok(characters);
    }

    @Operation(summary = "Get character by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404",
                    description = " No character found with provided ID.",
                    content = @Content)
    })
    @GetMapping("{id}") // GET: localhost:8080/characters/{id}
    public ResponseEntity<CharacterDto> getById(@PathVariable int id) {
        CharacterDto characterDto = characterMapper.characterToCharacterDto(characterService.findById(id));
        return ResponseEntity.ok(characterDto);
    }

    @Operation(summary = "Create a new character.")
    @PostMapping //POST localhost:8080/characters
    public ResponseEntity add(@RequestBody CharacterDto dto) {
        MovieCharacter movieCharacter = characterMapper.characterDtoToCharacter(dto);
        movieCharacter = characterService.add(movieCharacter);
        URI location = URI.create("characters/" + movieCharacter.getCharacter_id());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a characters values by ID.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") // PUT: localhost:8080/characters/{id}
    public ResponseEntity update(@RequestBody CharacterDto dto, @PathVariable int id) {
        // Validates if body is correct
        if (id == dto.getCharacter_id()) {
            MovieCharacter movieCharacter = characterMapper.characterDtoToCharacter(dto);
            characterService.update(movieCharacter);
            URI location = URI.create("characters/" + movieCharacter.getCharacter_id());
            return ResponseEntity.created(location).build();
        }
        return ResponseEntity.noContent().build();
    }

    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully deleted.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Character not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}") // DELETE: localhost:8080/characters/{id}
    public ResponseEntity delete(@PathVariable int id) {
        characterService.deleteById(id);
        return ResponseEntity.noContent().build();
    }
}