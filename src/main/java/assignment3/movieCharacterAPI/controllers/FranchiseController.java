package assignment3.movieCharacterAPI.controllers;

import assignment3.movieCharacterAPI.mappers.CharacterMapper;
import assignment3.movieCharacterAPI.mappers.FranchiseMapper;
import assignment3.movieCharacterAPI.mappers.MovieMapper;
import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.CharacterDto;
import assignment3.movieCharacterAPI.models.dto.FranchiseDto;
import assignment3.movieCharacterAPI.models.dto.MovieDto;
import assignment3.movieCharacterAPI.services.franchiseService.FranchiseService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.sql.Array;
import java.util.*;

@RestController
@RequestMapping(path = "franchises")
public class FranchiseController {

    private final FranchiseService franchiseService;

    private final FranchiseMapper franchiseMapper;
    private final MovieMapper movieMapper;
    private final CharacterMapper characterMapper;


    public FranchiseController(FranchiseService franchiseService, FranchiseMapper franchiseMapper, MovieMapper movieMapper, CharacterMapper characterMapper) {
        this.franchiseService = franchiseService;
        this.franchiseMapper = franchiseMapper;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }
    @Operation(summary = "Get all franchises.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No franchises found.",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<FranchiseDto>> getAll() {
        Collection<FranchiseDto> franchises = franchiseMapper.franchiseToFranchiseDTO(franchiseService.findAll());
        return ResponseEntity.ok(franchises);
    }

    @Operation(summary = "Get franchise by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Franchise.class))}),
            @ApiResponse(responseCode = "404",
                    description = " No franchise found with provided ID.",
                    content = @Content)
    })
    @GetMapping("{id}") //GET localhost:8080/franchises/{id}
    public ResponseEntity<FranchiseDto> getById(@PathVariable int id){
        FranchiseDto franchiseDto = franchiseMapper.franchiseToFranchiseDto(franchiseService.findById(id));
        return ResponseEntity.ok(franchiseDto);
    }

    @Operation(summary = "Create a new franchise.")
    @PostMapping //POST localhost:8080/franchises
    public ResponseEntity add(@RequestBody FranchiseDto dto){
        Franchise franchise = franchiseMapper.franchiseDtoToFranchise(dto);
        franchise = franchiseService.add(franchise);
        URI location = URI.create("franchises/" + franchise.getFranchise_id());
        return ResponseEntity.created(location).build();
    }

    @Operation(summary = "Update a franchisess values by ID.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Character successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}") //PUT localhost:8080/franchises/{id}
    public ResponseEntity update(@RequestBody FranchiseDto dto,@PathVariable int id){
        if (id == dto.getFranchise_id()){
            Franchise franchise = franchiseMapper.franchiseDtoToFranchise(dto);
            franchise = franchiseService.update(franchise);
            URI location = URI.create("franchises/" + franchise.getFranchise_id());
            return ResponseEntity.created(location).build();
        }
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a franchise by ID.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Franchise successfully deleted.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Franchise not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}") //DELETE localhost:8080/franchises/{id}
    public ResponseEntity delete(@PathVariable int id){
        franchiseService.deleteById(id);
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Get all movies in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No movies found.",
                    content = @Content)
    })
    @GetMapping("{id}/movies") //GET localhost:8080/franchises/{id}/movies
    public ResponseEntity<Collection<MovieDto>> getMoviesInFranchise(@PathVariable int id){
        Set<Movie> movies = franchiseService.getMoviesByFranchiseId(id);
        return ResponseEntity.ok(movieMapper.movieToMovieDto(movies));
    }


    @Operation(summary = "Get all characters in a franchise.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Character.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No characters found.",
                    content = @Content)
    })
    @GetMapping("{id}/characters") //GET localhost:8080/franchises/{id}/characters
    public ResponseEntity<Collection<CharacterDto>> getCharactersInFranchise(@PathVariable int id) {
        Set<MovieCharacter> movieCharacters = franchiseService.getCharactersByFranchiseId(id);
        return ResponseEntity.ok(characterMapper.characterToCharacterDTO(movieCharacters));
    }

    @PutMapping("{id}/movies") //PUT localhost:8080/franchises/{id}/movies
    public ResponseEntity updateMoviesInFranchise(@RequestBody int[] movie_ids, @PathVariable int id){
        franchiseService.updateMovieFranchiseByFranchiseId(movie_ids, id);
        URI location = URI.create("franchises/" + id + "/movies" );
        return ResponseEntity.created(location).build();
    }
}
