package assignment3.movieCharacterAPI.controllers;

import assignment3.movieCharacterAPI.mappers.CharacterMapper;
import assignment3.movieCharacterAPI.mappers.MovieMapper;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.CharacterDto;
import assignment3.movieCharacterAPI.models.dto.MovieDto;
import assignment3.movieCharacterAPI.services.movieService.MovieService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.util.Collection;
import java.util.Set;

@RestController
@RequestMapping(path = "movies")
public class MovieController {
    private final MovieService movieService;
    private final MovieMapper movieMapper;

    private final CharacterMapper characterMapper;

    public MovieController(MovieService movieService, MovieMapper movieMapper, CharacterMapper characterMapper) {

        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.characterMapper = characterMapper;
    }

    @Operation(summary = "Get all movies.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No movies found.",
                    content = @Content)
    })
    @GetMapping
    public ResponseEntity<Collection<MovieDto>> getAll() {
        Collection<MovieDto> movies = movieMapper.movieToMovieDto(movieService.findAll());
        return ResponseEntity.ok(movies);
    }

    @Operation(summary = "Get a movie by id.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = Movie.class))}),
            @ApiResponse(responseCode = "404",
                    description = " No movie found with provided ID.",
                    content = @Content)
    })
    @GetMapping("{id}")
    public ResponseEntity<MovieDto> getById(@PathVariable int id) {
        MovieDto movieDto = movieMapper.movieToMovieDto(movieService.findById(id));
        return ResponseEntity.ok(movieDto);
    }


    @Operation(summary = "Create a new movie.")
    @PostMapping
    public ResponseEntity add(@RequestBody MovieDto dto) {
       Movie movie = movieMapper.movieDtoToMovie(dto);
       movie = movieService.add(movie);
       URI location = URI.create("movies/" + movie.getMovie_id());
       return ResponseEntity.created(location).build();
       // return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @Operation(summary = "Update the information of a movie.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully updated",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @PutMapping("{id}")
    public ResponseEntity update(@RequestBody MovieDto dto, int id) {
        if(id == dto.getMovie_id()) {
            Movie movie = movieMapper.movieDtoToMovie(dto);
            movie = movieService.update(movie);
        }
        return ResponseEntity.noContent().build();
    }

    @Operation(summary = "Delete a movie by id.")
    @ApiResponses( value = {
            @ApiResponse(responseCode = "204",
                    description = "Movie successfully deleted.",
                    content = @Content),
            @ApiResponse(responseCode = "400",
                    description = "Malformed request",
                    content = @Content),
            @ApiResponse(responseCode = "404",
                    description = "Movie not found with supplied ID",
                    content = @Content)
    })
    @DeleteMapping("{id}")
    public ResponseEntity delete(@RequestBody int id) {
            movieService.deleteById(id);
            return ResponseEntity.noContent().build();
        }


    @Operation(summary = "Get all characters in a movie.")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200",
                    description = "Success",
                    content = { @Content(mediaType = "application/json",
                            schema = @Schema(implementation = MovieCharacter.class))}),
            @ApiResponse(responseCode = "404",
                    description = "No characters found.",
                    content = @Content)
    })
    @GetMapping("{id}/characters")
    public ResponseEntity<Collection<CharacterDto>> getMoviesInFranchise(@PathVariable int id){
        Set<MovieCharacter> characters = movieService.getCharactersByMovieId(id);
        return ResponseEntity.ok(characterMapper.characterToCharacterDTO(characters));
    }

    @PutMapping("{id}/characters") //PUT localhost:8080/movies/{id}/characters
    public ResponseEntity updateMoviesInFranchise(@RequestBody int[] character_ids, @PathVariable int id){
        movieService.updateCharactersByMovieId(character_ids, id);
        URI location = URI.create("movies/" + id + "/characters" );
        return ResponseEntity.created(location).build();
    }

}



