package assignment3.movieCharacterAPI.repositories;

import assignment3.movieCharacterAPI.models.Franchise;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Repository
public interface FranchiseRepository extends JpaRepository<Franchise, Integer> {
    @Modifying
    @Query(value = "SELECT fm.movie_id from franchise_movies as fm where franchise_id = ?", nativeQuery = true)
    Set<Integer> getMovieIdsByFranchiseId(int id);

    @Modifying
    @Query(value = "SELECT fm.character_id from character_franchises as fm where franchise_id = ?", nativeQuery = true)
    Set<Integer> getCharacterIdsByFranchiseId(int id);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "insert into franchise_movies (franchise_id, movie_id) values (?, ?);", nativeQuery = true)
    void updateMovieFranchiseById(int franchise_id, int movie_id);
}
