package assignment3.movieCharacterAPI.repositories;

import assignment3.movieCharacterAPI.models.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Integer> {

    @Modifying
    @Query(value = "SELECT mcm.character_id from movie_character_movies as mcm where movie_id = ?", nativeQuery = true)
    Set<Integer> getCharactersIdsByMovieId(int id);

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Modifying
    @Query(value = "insert into movie_character_movies (character_id, movie_id) values (?, ?)", nativeQuery = true)
    void updateCharacterByMovieId(int character_id, int movie_id);
}
