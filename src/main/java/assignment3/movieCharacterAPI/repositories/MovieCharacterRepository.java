package assignment3.movieCharacterAPI.repositories;

import assignment3.movieCharacterAPI.models.MovieCharacter;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieCharacterRepository extends JpaRepository<MovieCharacter, Integer> {
}
