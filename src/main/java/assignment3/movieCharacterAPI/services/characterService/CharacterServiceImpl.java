package assignment3.movieCharacterAPI.services.characterService;

import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.repositories.MovieCharacterRepository;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class CharacterServiceImpl implements CharacterService{

    private final MovieCharacterRepository movieCharacterRepository;

    public CharacterServiceImpl(MovieCharacterRepository movieCharacterRepository) {
        this.movieCharacterRepository = movieCharacterRepository;
    }

    @Override
    public MovieCharacter findById(Integer id) {
        return movieCharacterRepository.findById(id).get();
    }

    @Override
    public Collection<MovieCharacter> findAll() {
        return movieCharacterRepository.findAll();
    }

    @Override
    public MovieCharacter add(MovieCharacter movieCharacter) {
        return movieCharacterRepository.save(movieCharacter);
    }

    @Override
    public MovieCharacter update(MovieCharacter movieCharacter) {
        return movieCharacterRepository.save(movieCharacter);
    }

    @Override
    public void deleteById(Integer id) {
        if (movieCharacterRepository.existsById(id)){
            movieCharacterRepository.deleteById(id);
        }
    }

    @Override
    public void delete(MovieCharacter entity) {

    }
}
