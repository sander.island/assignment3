package assignment3.movieCharacterAPI.services.characterService;

import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.services.CrudService;
import org.springframework.stereotype.Service;

@Service
public interface CharacterService extends CrudService<MovieCharacter, Integer> {
    //business logic
}
