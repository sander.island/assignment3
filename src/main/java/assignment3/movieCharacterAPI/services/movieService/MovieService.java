package assignment3.movieCharacterAPI.services.movieService;

import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.services.CrudService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Set;

@Service
public interface MovieService extends CrudService<Movie, Integer> {

    Set<MovieCharacter> getCharactersByMovieId(int id);
    void updateCharactersByMovieId(int[] character_ids, int movie_id);
}
