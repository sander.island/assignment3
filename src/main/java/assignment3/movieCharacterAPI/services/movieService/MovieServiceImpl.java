package assignment3.movieCharacterAPI.services.movieService;

import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.repositories.MovieCharacterRepository;
import assignment3.movieCharacterAPI.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class MovieServiceImpl implements MovieService{

    private final MovieRepository movieRepository;
    private final MovieCharacterRepository movieCharacterRepository;

    public MovieServiceImpl(MovieRepository movieRepository, MovieCharacterRepository movieCharacterRepository) {
        this.movieRepository = movieRepository;
        this.movieCharacterRepository = movieCharacterRepository;
    }

    @Override
    public Movie findById(Integer id) {
        return movieRepository.findById(id).get();
    }

    @Override
    public Collection<Movie> findAll() {

        return movieRepository.findAll();
    }



    @Override
    public Movie add(Movie movie) {

        return movieRepository.save(movie);
    }

    @Override
    public Movie update(Movie movie) {
        return movieRepository.save(movie);
    }

    @Override
    public void deleteById(Integer id) {
        if(movieRepository.existsById(id)) {
            movieRepository.deleteById(id);
        }
    }

    @Override
    public void delete(Movie entity) {
    }

    @Override
    public Set<MovieCharacter> getCharactersByMovieId(int id) {
        Set<Integer> ids = movieRepository.getCharactersIdsByMovieId(id);
        return ids.stream().map(i -> movieCharacterRepository.findById(i).get()).collect(Collectors.toSet());
    }

    @Override
    public void updateCharactersByMovieId(int[] character_ids, int movie_id) {
        for (int id : character_ids){
            try{
                movieRepository.updateCharacterByMovieId(id, movie_id);
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }

}
