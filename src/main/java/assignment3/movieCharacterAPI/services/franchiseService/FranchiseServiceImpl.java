package assignment3.movieCharacterAPI.services.franchiseService;

import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.repositories.FranchiseRepository;
import assignment3.movieCharacterAPI.repositories.MovieCharacterRepository;
import assignment3.movieCharacterAPI.repositories.MovieRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collection;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class FranchiseServiceImpl implements FranchiseService{

    private final FranchiseRepository franchiseRepository;

    private final MovieRepository movieRepository;

    private final MovieCharacterRepository movieCharacterRepository;

    public FranchiseServiceImpl(FranchiseRepository franchiseRepository, MovieRepository movieRepository, MovieCharacterRepository movieCharacterRepository) {
        this.franchiseRepository = franchiseRepository;
        this.movieRepository = movieRepository;
        this.movieCharacterRepository = movieCharacterRepository;
    }

    @Override
    public Franchise findById(Integer id) {
        return franchiseRepository.findById(id).get();
    }

    @Override
    public Collection<Franchise> findAll() {
        return franchiseRepository.findAll();
    }

    @Override
    public Franchise add(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }

    @Override
    public Franchise update(Franchise franchise) {
        return franchiseRepository.save(franchise);
    }

    @Override
    public void deleteById(Integer id) {
        if (franchiseRepository.existsById(id)){
            franchiseRepository.deleteById(id);
        }
    }

    @Override
    public void delete(Franchise entity) {}

    @Override
    public Set<Movie> getMoviesByFranchiseId(int id){
        Set<Integer> ids = franchiseRepository.getMovieIdsByFranchiseId(id);
        return ids.stream().map(i -> movieRepository.findById(i).get()).collect(Collectors.toSet());
    }

    @Override
    public Set<MovieCharacter> getCharactersByFranchiseId(int id){
        Set<Integer> ids = franchiseRepository.getCharacterIdsByFranchiseId(id);
        return ids.stream().map(i -> movieCharacterRepository.findById(i).get()).collect(Collectors.toSet());
    }

    @Override
    public void updateMovieFranchiseByFranchiseId(int[] movie_ids, int franchise_id){
        for (int id : movie_ids){
            try{
                franchiseRepository.updateMovieFranchiseById(franchise_id, id);
            }
            catch (Exception e){
                System.out.println(e.getMessage());
            }
        }
    }
}
