package assignment3.movieCharacterAPI.services.franchiseService;

import assignment3.movieCharacterAPI.models.Franchise;
import assignment3.movieCharacterAPI.models.Movie;
import assignment3.movieCharacterAPI.models.MovieCharacter;
import assignment3.movieCharacterAPI.models.dto.MovieDto;
import assignment3.movieCharacterAPI.services.CrudService;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.Set;

public interface FranchiseService extends CrudService<Franchise, Integer> {
    //Business logic
    public Set<Movie> getMoviesByFranchiseId(int id);

    public Set<MovieCharacter> getCharactersByFranchiseId(int id);
    public void updateMovieFranchiseByFranchiseId(int[] movie_ids, int franchise_id);
}
