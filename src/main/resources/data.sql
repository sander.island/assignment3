INSERT INTO Movie
(movie_title, genre, release_year, director, picture_url, trailer_url)
VALUES
('Iron Man', 'Action', 2008, 'Jon Favreau', 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSzFXKSHWb3-5LAMWLxGAB5HzqsefGX4eYINaSHLd9JDNRu-LiM', 'IRON MAN TRAILER LINK'),
('The Lord of the Rings: The Fellowship of the Ring', 'Fantasy', 2001, 'Peter Jackson', 'https://i-viaplay-com.akamaized.net/viaplay-prod/606/408/1460127006-8faa6035eafd4487259fb06def95945ed739ede0.jpg?width=400&height=600', 'https://www.youtube.com/watch?v=xkiiEjz2O40&ab_channel=Upscaled'),
('The Lord of the Rings: Two Towers', 'Fantasy', 2002, 'Peter Jackson', 'https://m.media-amazon.com/images/M/MV5BZGMxZTdjZmYtMmE2Ni00ZTdkLWI5NTgtNjlmMjBiNzU2MmI5XkEyXkFqcGdeQXVyNjU0OTQ0OTY@._V1_FMjpg_UX1000_.jpg', 'https://www.youtube.com/watch?v=s-CfFX4v_Lw&ab_channel=TrailerWorld'),
('The Lord of the Rings: The Return of the King', 'Fantasy', 2003, 'Peter Jackson', 'https://m.media-amazon.com/images/M/MV5BNzA5ZDNlZWMtM2NhNS00NDJjLTk4NDItYTRmY2EwMWZlMTY3XkEyXkFqcGdeQXVyNzkwMjQ5NzM@._V1_.jpg', 'https://www.youtube.com/watch?v=KVApkGQI1ow&ab_channel=Pajasek99'),
('The Incredibles', 'Animation', 2004, 'Brad Bird', 'https://m.media-amazon.com/images/M/MV5BMTY5OTU0OTc2NV5BMl5BanBnXkFtZTcwMzU4MDcyMQ@@._V1_.jpg', 'https://www.youtube.com/watch?v=SOKY7XyOHTA&ab_channel=AnimationTrailers'),
('Harry Potter and the Philosopher''s Stone', 'Fantasy', 2001, 'Chris Colombus', 'https://m.media-amazon.com/images/M/MV5BMzkyZGFlOWQtZjFlMi00N2YwLWE2OWQtYTgxY2NkNmM1NjMwXkEyXkFqcGdeQXVyNjY1NTM1MzA@._V1_.jpg', 'https://www.youtube.com/watch?v=VyHV0BRtdxo&ab_channel=RottenTomatoesClassicTrailers'),
('Star Wars: A New Hope', 'Fantasy', 1977, 'George Lucas', 'https://kbimages1-a.akamaihd.net/ea6a1631-34e8-4369-b777-cf342521d3e0/1200/1200/False/a-new-hope-star-wars-episode-iv.jpg', 'https://www.youtube.com/watch?v=Ib_4A2CG2Ag&ab_channel=AHRIMAN'),
('Kill Buljo', 'Comedy', 2007, 'Tommy Wirkola', 'https://m.media-amazon.com/images/M/MV5BNzFjOTY0NGEtMzljYy00NmI5LTg0OGMtYjQxM2Y4ZDAwMmVkXkEyXkFqcGdeQXVyMjI4NTYxMTA@._V1_.jpg', 'https://www.youtube.com/watch?v=o3Wxqm-08fg'),
('Saving Private Ryan', 'War/Action', 1998, 'Steven Spielberg', 'https://m.media-amazon.com/images/M/MV5BZjhkMDM4MWItZTVjOC00ZDRhLThmYTAtM2I5NzBmNmNlMzI1XkEyXkFqcGdeQXVyNDYyMDk5MTU@._V1_FMjpg_UX1000_.jpg', 'https://www.youtube.com/watch?v=1AEqQEG8DW0&ab_channel=KloklineCinema');

INSERT INTO Franchise
(franchise_name, description)
VALUES
('Marvel Cinematic Universe', 'Marvel heroes saves the world'),
('The Lord of the Rings', 'Tolkiens world and their mysterious happenings'),
('The Incredibles', 'An incredible family have to save the world from a dangerous kid'),
('Harry Potter', 'Wizard stories from Hogwarts'),
('Star Wars', 'Legends about The Force and the Jedi'),
('Cars', 'Lightning McQueen and his dream of winning the Piston Cup'),
('Toy Story', 'Toys become alive in Andy''s room'),
('Død Snø', 'Nazi zombies comes to life in northern Norway');

INSERT INTO Movie_Character
(full_name, alias, gender, picture_url)
VALUES
('Anthony Edward Stark', 'Iron Man', 'Male', 'https://static.wikia.nocookie.net/marvelstudios/images/b/b3/Ironman-endgameprofile.jpg/revision/latest?cb=20191226115453&path-prefix=fr'),
('Frodo Baggins', 'Frodo', 'Male', 'https://static.wikia.nocookie.net/lotr/images/1/1a/FotR_-_Elijah_Wood_as_Frodo.png/revision/latest?cb=20130313174543'),
('Samwise Gamgee', 'Sam', 'Male', 'https://static.wikia.nocookie.net/lotr/images/9/9d/Samwise_the_Brave.jpg/revision/latest?cb=20090109192224'),
('Legolas Greenleaf', 'Legolas', 'Male', 'https://static.wikia.nocookie.net/lotr/images/5/5e/Legolas_The_Hobbit.jpg/revision/latest?cb=20121117114948'),
('Helen Parr', 'Elastica', 'Female', 'https://static.wikia.nocookie.net/disney/images/3/3a/Profile_-_Helen_Parr.jpeg/revision/latest?cb=20200205215542'),
('Harry Potter', 'The boy who lived', 'Male', 'https://static.wikia.nocookie.net/hpbok/images/1/12/Harrypotter1.jpg/revision/latest?cb=20100726152215&path-prefix=sv'),
('Anakin Skywalker', 'Darth Vader', 'Male', 'https://static.feber.se/article_images/50/48/66/504866.jpg'),
('Bruce Wayne', 'Batman', 'Male', 'https://cdn.mos.cms.futurecdn.net/2NBcYamXxLpvA77ciPfKZW-1200-80.jpg');

INSERT INTO movie_character_movies
(character_id, movie_id)
VALUES
(1, 1),
(2, 2),
(2, 3),
(2, 4),
(3, 2),
(3, 3),
(3, 4),
(4, 2),
(4, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7);

INSERT INTO character_franchises(character_id, franchise_id)
VALUES
(1, 1),
(2, 2),
(3, 2),
(4, 2),
(5, 3),
(6, 4),
(7, 5);

INSERT INTO franchise_movies(franchise_id, movie_id)
values
(1, 1),
(2, 2),
(2, 3),
(2, 4),
(3, 5),
(4, 6),
(5, 7);