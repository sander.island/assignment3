# Assignment 3

### Participants
* Martin Mikkelsen
* Isar Buzza
* Sander Island

## Description
This project is a group project for assignment 3 in Noroff's Java Fullstack course. In this project, we have created a database consisting of movies, characters and franchises, and our task is to extract and alter/update desired data.

## Project task
You have been tasked with creating a datastore and interface to store and manipulate movie characters. It may be
expanded over time to include other digital media, but for now, stick to movies.

The application should be constructed in Spring Web and comprise of a database made in PostgreSQL through Hibernate
with a RESTful API to allow users to manipulate the data. The database will store information about characters, movies
they appear in, and the franchises these movies belong to. This should be able to be expanded on.


Full CRUD is expected for Movies, Characters, and Franchises. You can do proper deletes, just ensure related
data is not deleted – the foreign keys can be set to null (business logic). This means that Movies can have no
Characters, and Franchises can have no Movies.

When adding new resources, do not add related data at the same time (leave null), this is deferred to an
update. The requirements for this are detailed below.

### Endpoints
#### Character endpoins
```
GET:
characters/
characters/{id}

POST:
characters/

PUT:
characters/{id}

DELETE:
characters/{id}
```

#### Movie endpoins
```
GET:
movies/
movies/{id}
movies/{id}/characters

POST:
movies/

PUT:
movies/{id}

DELETE:
movies/{id}
```

#### Franchise endpoins
```
GET:
franchises/
franchises/{id}
franchises/{id}/movies
franchises/{id}/characters

POST:
franchises/

PUT:
franchises/{id}
franchises/{id}/movies

DELETE:
franchises/{id}
```

### Tools
* IntelliJ has been used for coding
* pgAdmin has been used for handling the database
* Postgres SQL
* Postman for testing functionality
* Docker for container
* Heroku for deployment
* Swagger for API documentation


### Git usage
We had our own branches where each group member worked on their given tasks.
